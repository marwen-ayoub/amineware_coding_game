const fs = require('fs');

const inputFile = './input.txt';
const outputFile = './output.txt';

fs.readFile(inputFile, 'utf8', (err, data) => {
    if (err) {
        console.error("Error while reading the input file text !!!");
        return;
    }
    const EmployeesCodes = data.trim().split('\n');

    EmployeesCodes.sort((code, nextCode) => {
        const firstLetterComparison = code[0].localeCompare(nextCode[0]);
        if (firstLetterComparison !== 0) {
            return firstLetterComparison;
        }

        const lastLetterComparison = code[9].localeCompare(nextCode[9]);

        if (lastLetterComparison !== 0) {
            return lastLetterComparison;
        }

        const numA = parseInt(code.slice(1, -1), 10);
        const numB = parseInt(nextCode.slice(1, -1), 10);

        return numA - numB;
    });

    fs.writeFile(outputFile, EmployeesCodes.join('\n'), 'utf8', (err) => {
        if (err) {
            console.error("Error occurred while writing to the output file !!!");
        } else {
            console.log('Yaay, We did it !!!');
        }
    });
});